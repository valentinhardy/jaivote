<?php
require_once('./views/View.php');

class ControllerConnexion {
    private $_connexionManager;
    private $_view;

    public function connexion()
    {
        $this->_connexionManager = new ConnexionManager;
        $connexion = $this->_connexionManager->getConnexion();

        $this->_view = new View('Connexion');
        $this->_view->generate(array('connexion' => $connexion));
    }
}
