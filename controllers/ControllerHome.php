<?php
require_once('./views/View.php');

class ControllerHome {

    private $_votesManager;
    private $_usersManager;
    private $_tagManager;
    private $_view;

    public function home()
    {

        //Votes
        $this->_votesManager = new VotesManager;
        $votes = $this->_votesManager->getVotes();
        $totalVotes = $this->_votesManager->getTotalVotes();
        $totalLikes = $this->_votesManager->getTotalLikes();
        $totalDislikes = $this->_votesManager->getTotalDislikes();
        $totalDislikes = $this->_votesManager->getTotalDislikes();

        // //Users
        // $this->_usersManager = new UsersManager;
        // $totalUsers = $this->_usersManager->getCustomers();
        // $lastUser = $this->_usersManager->getLastUser();

        $this->_tagManager = new TagsManager;
        $tags = $this->_tagManager->getTagsById();
    


        $this->_view = new View('Home');

        $this->_view->generate(array(
                'votes' => $votes,
                'totalVotes' => $totalVotes,
                'totalLikes' => $totalLikes,
                'totalDislikes' => $totalDislikes,
                // 'totalUsers' => $totalUsers,
                // 'lastUser' => $lastUser,
                'tags' => $tags
            ));
        }
        
    }