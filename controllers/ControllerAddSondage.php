<?php
require_once('./views/View.php');

class ControllerAddSondage {
    private $_AddSondageManager;
    private $_view;

    public function Sondage(){

        // Creer la vue
        $this->_sondageManager = new AddSondageManager();
        $sondage = $this->_sondageManager->createSondage(); 
        $this->_userManager = new UsersManager();
    
        $this->_view = new View('Propose');
        $this->_view->generate(array('Propose' => $sondage));
    }

    // Soumettre un vote
    public function createVote(){
        $this->_voteManager = new VotesManager();
        $content = htmlspecialchars(trim($_POST['content']));
        $username = htmlspecialchars(trim($_POST['username']));
        $email = htmlspecialchars(trim($_POST['email']));

        $this->_voteManager->sendVote($username,$content,$email); 
    }

}
