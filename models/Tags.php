<?php

class Tags {

    private $_id;
    private $_username;
    private $_surname;

    public function __construct(array $data){
        $this->hydrate($data);
    }

    //
    public function hydrate(array $data){
        foreach($data as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method))
            $this->$method($value);
        }
    }
    // ID
    public function setId($id){
        $id = (int) $id;
        if($id > 0){
            $this->_id = $id;
        }
    }
}