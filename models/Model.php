<?php

// Methodes universelles

    class Model { //ne peut pas être instanciée

        private static $bdd;

        // Connexion bdd
        public function setBdd(){
            $dbhost = 'localhost';
            $dbname = 'jaivote';
            $dbuser = 'root';
            $dbpswd = '';
            self::$bdd= new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpswd,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        }

        // Fonction qui recupere la connexion à la bdd
        public function getBdd(){
            if(self::$bdd == null){
                $this->setBdd();
            }
            return self::$bdd;
        }

        //RECUP les articles
        protected function getAll($table,$obj){
            $var = [];
            $req = $this->getBdd()->prepare("SELECT * FROM ".$table." ORDER BY id desc");
            $req->execute();

            while($data = $req->fetch(PDO::FETCH_ASSOC)){
                $var[] = new $obj($data);
            }

            return $var;
            $req->closeCursor();
        }

        //RECUP un article
        protected function getOne($id){
            //requête
            $req = $this->getBdd()->prepare("SELECT * FROM votes WHERE posted='1' AND id=? ");
            $req->execute(array($id));

            $result = $req->fetchObject();

            return $result;

        }



    }
