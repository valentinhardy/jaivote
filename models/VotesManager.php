<?php

class VotesManager extends Model {

    //recupere tous les votes
    public function getVotes(){
        $this->getBdd(); //function article manager
        return $this->getAll('votes', 'Votes');
    }

    //Recupere un vote en fonction de l'id
    public function getVote($id) {
        $this->getBdd();
        return $this->getOne($id);
    }
    
    // recupere le nombre de votes total
    function getTotalVotes(){
        //requête
        $req = $this->getBdd()->prepare("SELECT SUM(votes) AS total FROM votes");
        $req->execute();

        $result = $req->fetchObject();

        return $result;
    }

    // recupere le nombre de votes total
    function getTotalLikes(){
        //requête
        $req = $this->getBdd()->prepare("SELECT SUM(likes) AS total FROM votes");
        $req->execute();

        $result = $req->fetchObject();

        return $result;
    }

    // recupere le nombre de votes total
    function getTotalDislikes(){
        //requête
        $req = $this->getBdd()->prepare("SELECT SUM(dislikes) AS total FROM votes");
        $req->execute();

        $result = $req->fetchObject();

        return $result;
    }

      // envoi les votes sur la bdd
      public function sendVote($username,$content,$email){

        
        $sentvote = array(
            'email'   => $email,
            'username' => $username,
            'content' => $content
        );
        $sql = "INSERT INTO votes(email,username,content,date) VALUES (:email, :username, :content, NOW())";
        
        $req = $this->getBdd()->prepare($sql);
        $req->execute($sentvote);
    } 

}
