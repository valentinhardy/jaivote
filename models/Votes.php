<?php

class Votes {

    private $_id;
    private $_content;
    private $_date;
    private $_likes;
    private $_dislikes;
    private $_image;
    private $_votes;
    private $_username;

    public function __construct(array $data){
        $this->hydrate($data);
    }

    //
    public function hydrate(array $data){
        foreach($data as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method))
            $this->$method($value);
        }
    }
    // ID
    public function setId($id){
        $id = (int) $id;
        if($id > 0){
            $this->_id = $id;
        }
    }
    //get ID
    public function id(){
        return $this->_id;
    }

    //votes
    public function setvotes($votes){
        $votes = (int) $votes;
        if($votes > 0){
            $this->_votes = $votes;
        }
    }
    public function votes(){
        return $this->_votes;
    }


    //likes
    public function setLikes($likes){
        $likes = (int) $likes;
        if($likes > 0){
            $this->_likes = $likes;
        }
    }
    public function likes(){
        return $this->_likes;
    }

    //dislikes
    public function setDislikes($dislikes){
        $dislikes = (int) $dislikes;
        if($dislikes > 0){
            $this->_dislikes = $dislikes;
        }
    }
    public function dislikes(){
        return $this->_dislikes;
    }


    //username
    public function setUsername($username){
        if(is_string($username)){
            $this->_username = $username;
        }
    }
    public function username(){
        return $this->_username;
    }

    //content
    public function setContent($content){
        if(is_string($content)){
            $this->_content = $content;
        }
    }
    public function content(){
        return $this->_content;
    }

    //date
    public function setDate($date){
        $this->_date = $date;
    }
    public function Date(){
        return $this->_date;
    }

    //medias
    public function setImage($image){
        if(is_string($image)){
            $this->_image = $image;
        }
    }
    public function image(){
        return $this->_image;
    }

}
