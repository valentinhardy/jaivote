<?php

class TagsManager extends Model {

    //recupere tous les utilisateurs
    function getTags(){
        $this->getBdd(); //function article manager
        return $this->getAll('tags', 'Tags');
    }

    //Recupere les tags par vote_id
    function getTagsById(){
    

        $req = $this->getBdd()->prepare("SELECT tags.category AS tag, votes.id AS vote, votes.content AS name_vote FROM tags, votes WHERE votes.id = tags.vote");
        $req->execute();

        while($rows = $req->fetchObject()){
            $result[] = $rows;
        }
        return $result;
    }
}
