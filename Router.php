<?php 


require_once('views/View.php');

class Router {

    private $control;
    private $view;

    public function routeReq(){
        try 
        {
            //chargement auto des classes // spl_autoload_register->repere le nom de la classe automatiquement
            spl_autoload_register(function($class){ 

                if(substr($class, 0, 10) === 'Controller'){
                    require_once('controllers/'.$class.'.php');
                } 
                else {
                require_once('models/'.$class.'.php');
                }

            });

            $url = '';
            // Controller est inclu selon l'action de l'utilisateur
            if(isset($_GET['url']))
            {   //Recup les parametres de manière separées par un / puis filtre 
                $url = explode('/', filter_var($_GET['url'], FILTER_SANITIZE_URL));
                
                switch ($_GET['url']) {
                    case 'post':
                        $_postController = new ControllerPost();
                        $_postController->post($_GET['id']); 
                        break;
                    case 'addComment': 
                        $_commentController = new ControllerPost();
                        if(is_numeric($_GET['id'])){
                            $_commentController->addComment($_GET['id']);
                        }
                    break;
                    case 'blog': 
                        $_blogController = new ControllerBlog();
                        $_blogController->articles();
                    break;
                    case 'home':
                        $_homeController = new ControllerHome();
                        $_homeController->home();
                    break;
                    default:
                        throw new Exception('Page introuvable');
                    break;
                }
            }
            else
            {
                require_once('controllers/ControllerHome.php');
                $this->control = new ControllerHome($url);
            }
        }
        //Gestion des erreurs
        catch(Exception $e)
        {
            $errorMsg = $e->getMessage();
            $this->_view = new View('Error');
            $this->_view->generate(array('errorMsg' => $errorMsg));
        }
    }
}
